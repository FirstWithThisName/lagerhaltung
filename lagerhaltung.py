#!/usr/bin/python3


# Lagerhaltungsmodell

verbrauchProTAG = 50
hoestStand = 800
lieferzeit_day = 5
sicherheitszuschlag_day = 4

sicherheitsbestand = verbrauchProTAG * sicherheitszuschlag_day
meldebestand = sicherheitsbestand + verbrauchProTAG * lieferzeit_day

bestelltag_day = (hoestStand - meldebestand) / verbrauchProTAG

liefertag = bestelltag_day + lieferzeit_day

print("sicherheitsbestand " + str(sicherheitsbestand))
print("meldebestand " + str(meldebestand))
print("bestelltag " + str(bestelltag_day))
print("lieferzeit " + str(lieferzeit_day))
print("liefertag " + str(liefertag))
