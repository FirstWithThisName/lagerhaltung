
class lagerModell:
    def __init__(self, verbrauchPerDay, hoestStand, lieferzeit, sicherheitsZuschlag):
        self.verbrauchPerDay = verbrauchPerDay
        self.hoestStand = hoestStand
        self.lieferzeit = lieferzeit
        self.sicherheitsZuschlag = sicherheitsZuschlag

    def getSicherheitsBestand(self):
        return self.verbrauchPerDay * self.sicherheitsZuschlag

    def getMeldeBestand(self):
        return self.getSicherheitsBestand() + self.verbrauchPerDay * self.lieferzeit

    def getBestellMenge(self):
        return self.hoestStand - self.getSicherheitsBestand()

    def getBestellTag(self):
        return (self.hoestStand - self.getMeldeBestand()) / self.verbrauchPerDay

    def getLieferTag(self):
        return (self.getBestellTag() + self.lieferzeit)

    def getBestandOnDay(self, day):
        return self.hoestStand - (self.verbrauchPerDay * (day % (self.getLieferTag() + 1)))
