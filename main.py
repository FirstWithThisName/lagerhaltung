import matplotlib.pyplot as plt
import layermodell

plt_hb = []
plt_meldeBestand = []
plt_menge = []
plt_sicherheitsBestand = []


days = 15
vpd = float(input("Verbrauch Pro Tag "))
hst = float(input("Höststand "))
lfz = float(input("lieferzeit "))
shz = float(input("Sicherheits Zuschlag "))

lm = layermodell.lagerModell(verbrauchPerDay=vpd, hoestStand=hst, lieferzeit=lfz, sicherheitsZuschlag=shz)

days = int(lm.getLieferTag() + (lm.getLieferTag() / 2))

for i in range(days):
    plt_hb.append(float(lm.hoestStand))
    plt_sicherheitsBestand.append(float(lm.getSicherheitsBestand()))
    plt_meldeBestand.append(float(lm.getMeldeBestand()))
    plt_menge.append(float(lm.getBestandOnDay(day=i)))

plt.plot(plt_hb, label="höchstbestand")
plt.plot(plt_sicherheitsBestand, label='sicherheitsbestand')
plt.plot(plt_menge, label="lagermenge")
plt.plot(plt_meldeBestand, label="meldebestand")

plt.legend()
plt.show()
